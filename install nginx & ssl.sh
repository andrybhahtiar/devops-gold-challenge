
# nginx
install nginx
$sudo apt update
$sudo apt install nginx

check nginx
$systemctl status nginx

manage
$sudo systemctl stop nginx
$sudo systemctl start nginx
$sudo systemctl restart nginx

install snapd
$sudo apt update
$sudo apt install snapd
$sudo snap install core
$sudo snap install core; sudo snap refresh core

install certbot
$sudo snap install --classic certbot
$sudo ln -s /snap/bin/certbot /usr/bin/certbot
$sudo certbot --nginx



